# Google Play Store Automation
Automation testing for Installation process from Google Play Store. Using Selenium webDriver-Java, through TestNG Framework.

The test should do the following steps:
1) Open website https://play.google.com/.
2) Login with saved email to Google Play Store.
3) Open **Apps** page.
4) Add the first and last games to **The Wishlist**.
5) Open **My Wishlist** and select one of the games added.
6) Install this game 
7) Write a review about this game **(Select 5 stars and TEST in the textfield)** and submit your review.
8) Back to the previous page and delete the comment you added.

**And this link is for a video that displays the previous steps and the test results report using TestNG Framework**

https://www.youtube.com/watch?v=b5vdNkMQ56o&feature=youtu.be


# Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 
See deployment for notes on how to deploy the project on a live system.
# Tools
* Selenium WebDriver
* Selenium Standalone Server
* TestNG Framework

# Requirements 
* Selenium chrom driver 3.3
* Selenium standalone driver 3.14
* Chrome and Chromedriver
* Java 1.8

# Installing
1) Install Java on your computer
         Download and install the Java Software Development Kit (JDK).
         
2) Install Eclipse IDE
         Download "Eclipse IDE for Java Developers". Be sure to choose correctly between Windows 32 Bit and 64 Bit versions. 
         
3) Download the Selenium Java Client Driver
         You will find client drivers for other languages there, but only choose the one for Java. 
         
4) Configure Eclipse IDE with WebDriver
* Launch the "eclipse.exe" file inside the "eclipse" folder, the executable should be located on C:\eclipse\eclipse.exe.
* When asked to select for a workspace, just accept the default location.

5) Install/Add TestNG plugin to Eclipse.

6) After downloading the project and Open it, Please Enter your valid Email and Password in **Config_Class**.
![Screenshot](https://gitlab.com/ayasadeksaleh/google-play-store-automation/blob/master/AutomatainChallenge_TestNG/automationTest.png)

7) Run **testng.xml** File.
