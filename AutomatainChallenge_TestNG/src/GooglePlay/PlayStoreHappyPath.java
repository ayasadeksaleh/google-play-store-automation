package GooglePlay;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class PlayStoreHappyPath {
    PlayStore playStore;
    
  @BeforeTest
	public void login() throws Exception {
		playStore = new PlayStore ();
		playStore.login(Config.UserName,Config.Password);
	}
   @Test 
	public void testHappyPathAddFirstAndLastGameToWhishListAndInstallThem() throws Exception {
	boolean expectedResult = true;
    boolean actualResult;
    actualResult = playStore.goToPlayListPage();
	Assert.assertEquals(actualResult, expectedResult);
        
    actualResult = playStore.addGameToWishListHappyPath();
    Assert.assertEquals(actualResult, expectedResult);
        
    actualResult = playStore.installGame();
    Assert.assertEquals(actualResult, expectedResult);
		
    actualResult = playStore.writeReview();
    Assert.assertEquals(actualResult, expectedResult);
    }
    
    @AfterTest 
    public void driverClosing ()
	{
    	playStore.DriverTerminate();
	}

}
