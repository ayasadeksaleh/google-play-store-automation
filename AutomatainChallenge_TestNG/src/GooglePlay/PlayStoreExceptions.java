package GooglePlay;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.Assert;

public class PlayStoreExceptions {
	PlayStore playStore;

	@Test(priority = 0)
	public void AlternatePath_InvalidEmail() throws InterruptedException {
		playStore = new PlayStore();
		boolean expectedResult = false; // swany
		boolean actualResult;
		actualResult = playStore.login("abc", Config.Password);
		Assert.assertEquals(actualResult, expectedResult, "INVALID EMAIL");
	}

	@Test(priority = 1)
	public void AlternatePath_InvalidPassword() throws InterruptedException {
		playStore = new PlayStore();
		boolean expectedResult = false;
		boolean actualResult;
		actualResult = playStore.login(Config.UserName, "123");
		Assert.assertEquals(actualResult, expectedResult, "INVALID PASSWORD");
	}

	@Test(priority = 2)
	public void testAlternatePath_FirstOrLastGameIsAlreadyAddedToWishList() throws InterruptedException {
		playStore = new PlayStore();
		playStore.login(Config.UserName, Config.Password);

		boolean expectedResult = true;
		boolean actualResult;

		actualResult = playStore.goToPlayListPage();
		Assert.assertEquals(actualResult, expectedResult);

		actualResult = playStore.addGameToWishListAlternatePath();
		Assert.assertEquals(actualResult, expectedResult);

		actualResult = playStore.installGame();
		Assert.assertEquals(actualResult, expectedResult);

		actualResult = playStore.writeReview();
		Assert.assertEquals(actualResult, expectedResult);
	}

	@Test(priority = 3)
	public void testAlternatePath_thereIsNoDeviceForInstallation() throws InterruptedException {
		playStore = new PlayStore();
		playStore.login(Config.UserName, Config.Password);
		boolean expectedResult = true;
		boolean expectedResultNotFound = false;
		boolean actualResult;
		actualResult = playStore.goToPlayListPage();
		Assert.assertEquals(actualResult, expectedResult);

		actualResult = playStore.addGameToWishListAlternatePath();
		Assert.assertEquals(actualResult, expectedResult);

		actualResult = playStore.NoDeviceForInstallation();
		Assert.assertEquals(actualResult, expectedResultNotFound);
	}

	@AfterTest
	public void driverClosing() {
		playStore.DriverTerminate();
	}

}
