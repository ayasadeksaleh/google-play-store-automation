package GooglePlay;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class PlayStore {

	WebDriver driver;

	PlayStore() {
		System.setProperty("webdriver.chrome.driver",
				"D:\\Testing\\GoogleGames_Automation\\Webdrivers\\Chrome\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://play.google.com/store");
		driver.manage().window().maximize();
	}

	public boolean login(String username , String password) throws InterruptedException {
		driver.findElement(By.id("gb_70")).click();
		driver.findElement(By.id("identifierId")).sendKeys(username);
		driver.findElement(By.id("identifierNext")).click();
		if (driver.findElements(By.cssSelector("div[class='GQ8Pzc']")).size()> 0)
			return false;
		
		driver.findElement(By.cssSelector("input type=\"password\"").name("password")).sendKeys(password);
		driver.findElement(By.className("CwaK9")).click();
		if (driver.findElements(By.cssSelector("div[class='xgOPLd']")).size()> 0)
			return false;
		Thread.sleep(2000);

		return true;
	}

	public boolean goToPlayListPage() throws InterruptedException {
	    Thread.sleep(2000);
		driver.findElement(By.cssSelector("a[href='/store/apps']")).click();
		driver.findElement(By.cssSelector("a[href=\"https://play.google.com/store/apps/collection/cluster?clp=ogooCAEaHAoWcmVjc190b3BpY18xQ1pYNWY0YkFEcxA7GAMqAggBUgIIAg%3D%3D:S:ANO1ljKYdUs&gsr=CiuiCigIARocChZyZWNzX3RvcGljXzFDWlg1ZjRiQURzEDsYAyoCCAFSAggC:S:ANO1ljICdng\"]")).click();
		return true;
	}

	public void FirstGameShouldBeAddedToWishList() {
		List<WebElement> li = driver.findElements(By.cssSelector("div[class='card-content id-track-click id-track-impression']"));
		for (int i = 0; i < li.size() - 1; i++) {
			li = driver.findElements(By.cssSelector("div[class='card-content id-track-click id-track-impression']"));
			li.get(i).click();
		
				if (driver.findElements(By.cssSelector("span[class='lmqfOc nSYH7e']")).size()> 0) {
					driver.findElement(By.cssSelector("span[class='lmqfOc nSYH7e']")).click();
					driver.navigate().back();
					break;
				} 
				else {
					driver.navigate().back();
				}
		}
	}

	public void LastGameShouldBeAddedToWishList() {
		List<WebElement> li = driver.findElements(By.cssSelector("div[class='card-content id-track-click id-track-impression']"));
		for (int i = li.size()-1 ; i>0 ; i--) {
			li = driver.findElements(By.cssSelector("div[class='card-content id-track-click id-track-impression']"));
			li.get(i).click();
		
				if (driver.findElements(By.cssSelector("span[class='lmqfOc nSYH7e']")).size()> 0) {
					driver.findElement(By.cssSelector("span[class='lmqfOc nSYH7e']")).click();
					driver.navigate().back();
					break;
				} 
				else {
					driver.navigate().back();
				}
		}
	}
	public boolean addGameToWishListAlternatePath() {
		FirstGameShouldBeAddedToWishList();
		LastGameShouldBeAddedToWishList();
		return true;
	}

	public boolean addGameToWishListHappyPath () throws InterruptedException
	{
		List<WebElement> li = driver.findElements(By.cssSelector("div[class='card-content id-track-click id-track-impression']"));
		li.get(0).click();
		driver.findElement(By.cssSelector("span[class='lmqfOc nSYH7e']")).click();
		driver.navigate().back();
		li = driver.findElements(By.cssSelector("div[class='card-content id-track-click id-track-impression']"));
		li.get(li.size()-1).click();
		driver.findElement(By.cssSelector("span[class='lmqfOc nSYH7e']")).click();
		driver.navigate().back();
		return true;
	}
	public boolean installGame() throws InterruptedException {
		driver.navigate().to("https://play.google.com/wishlist");
		//driver.findElement(By.cssSelector("a[href='https://play.google.com/wishlist']")).click();
		Thread.sleep(4000);
		List<WebElement> li2 = driver.findElements(By.cssSelector("div[class='card-content id-track-click id-track-impression']"));
		li2.get(0).click();
		driver.findElement(By.cssSelector("button[aria-label='Install']")).click();
		Thread.sleep(2000);
		driver.switchTo().frame(2);
		Thread.sleep(2000);
		driver.findElement(By.id("device-selector-container")).click();
		if (driver.findElements(By.cssSelector("div[class='device-selector-dropdown-children']")).size()> 0)
		{
		driver.findElement(By.cssSelector("button[class='device-selector-dropdown-child device-selected']")).click();
		}
		else 
			return false; 
		driver.findElement(By.id("purchase-ok-button")).click();
		driver.findElement(By.id("close-dialog-button")).click();
		driver.navigate().refresh();
		
		return true;
	}
	
	public boolean NoDeviceForInstallation() throws InterruptedException
	{
		driver.navigate().to("https://play.google.com/wishlist");
		Thread.sleep(4000);
		List<WebElement> li2 = driver.findElements(By.cssSelector("div[class='card-content id-track-click id-track-impression']"));
		li2.get(0).click();
		driver.findElement(By.cssSelector("button[aria-label='Install']")).click();
		Thread.sleep(2000);
		return false;
	}

	public boolean writeReview() throws InterruptedException {
		JavascriptExecutor jsx = (JavascriptExecutor) driver;
		jsx.executeScript("window.scrollBy(0,800)");
		driver.findElement(By.cssSelector("button[class*='LkLjZd ScJHi HPiPcc id-track-click']")).click();
		Thread.sleep(2000);
		driver.switchTo().frame(2);
		Thread.sleep(2000);
		List<WebElement> elements = driver.findElements(By.cssSelector("button[class='fifth-star']"));
		WebElement element = elements.get(1);
		element.click();
		driver.findElement(By.cssSelector("textarea[class='review-input-text-box write-review-comment']"))
				.sendKeys("TEST");
		driver.findElement(By.cssSelector("button[class='id-submit-review play-button apps']")).click();
		JavascriptExecutor jsx1 = (JavascriptExecutor) driver;
		jsx1.executeScript("window.scrollBy(0,450)");
		driver.findElement(By.cssSelector("span[class='DPvwYc NE4Eeb']")).click();
		
		return true;
	}

	public void DriverTerminate() {
		driver.close();
	}
}
